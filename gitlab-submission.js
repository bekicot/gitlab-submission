const axios = require('axios')
const snakeCase = require('lodash.snakecase')
const get = require('lodash.get')
const padStart = require('lodash.padstart')
const yaml = require('js-yaml')
const qs = require('querystring')
const crypto = require('crypto')
const util = require('util')

const logs = []
const log = (level, ...msgs) => {
  console.log(...msgs)
  msgs.forEach(msg => logs.push(`${level}: ${util.inspect(msg)}`))
}

const gitlab = token => {
  const client = axios.create({
    baseURL: 'https://gitlab.com/api/v4',
    headers: {
      'PRIVATE-TOKEN': token,
    },
  })

  return {
    createBranch(data) {
      const { project, ...attr } = data
      return client.post(`/projects/${project}/repository/branches`, attr)
    },

    createFile(data) {
      const { path, project, ...attr } = data
      return client.post(`/projects/${project}/repository/files/${path}`, attr)
    },

    createMR(data) {
      const { project, ...attr } = data
      return client.post(`/projects/${project}/merge_requests`, attr)
    },
  }
}

const createSubmission = (provider, project) => async data => {
  const {
    branch,
    author,
    email,
    message,
    title,
    description,
    file,
    content,
  } = data

  const projectEscaped = qs.escape(project)
  const path = qs.escape(file)

  log('info', 'Processing submission', data)

  log('info', 'Creating branch')
  await provider.createBranch({
    project: projectEscaped,
    ref: 'master',
    branch,
  })

  log('info', 'Creating file')
  await provider.createFile({
    author_name: author,
    author_email: email,
    commit_message: message,
    project: projectEscaped,
    branch,
    content,
    path,
  })

  log('info', 'Creating merge request')
  return provider.createMR({
    source_branch: branch,
    target_branch: 'master',
    project: projectEscaped,
    title,
    description,
  })
}

const sha256 = string => {
  return crypto
    .createHash('sha256', '')
    .update(string)
    .digest('hex')
}

const netlifyFormToSubmission = ({
  branch,
  author,
  email,
  title,
  description,
  path,
  excludeField,
}) => req => {
  const message = `Add new submission from: ${author}`

  const number = req.number
  const paddedNumber = padStart(number, 3, 0)
  const snakeCaseAuthor = snakeCase(author)
  const file = `${path}/${paddedNumber}.md`

  const data = { nomor: number, ...req.data, submission_id: req.id }

  excludeField.forEach(field => {
    delete data[field]
  })

  const frontmatter = yaml.dump(data)
  const content = `---\n${frontmatter}---\n`

  return {
    branch,
    author,
    email,
    message,
    title,
    description,
    file,
    content,
  }
}

const logWritter = (provider, project) => async () => {
  const date = new Date().toLocaleString()
  const projectEscaped = qs.escape(project)

  await provider.createFile({
    author_name: 'logger',
    author_email: 'logger@nobody.io',
    commit_message: `Triggered @ ${date}`,
    project: projectEscaped,
    branch: 'master',
    content: logs.join('\n'),
    path: `${date}.log`,
  })
}

let writeLog

module.exports = async ({ secrets, meta, body }, cb) => {
  log('info', 'New request', body)

  const token = secrets.GITLAB_TOKEN
  const project = meta.PROJECT
  const logProject = meta.LOG_PROJECT
  process.env.TZ = meta.TIMEZONE

  const provider = gitlab(token)
  const submit = createSubmission(provider, project)

  if (logProject) {
    writeLog = logWritter(provider, logProject)
  }

  const author = get(body.data, meta.FORM_AUTHOR_KEY)
  const title = get(body.data, meta.FORM_TITLE_KEY)
  const branchPrefix = meta.BRANCH_PREFIX || ''

  const netlifyForm = netlifyFormToSubmission({
    author,
    branch: branchPrefix + sha256(get(body.data, meta.FORM_BRANCH_KEY)),
    email: get(body.data, meta.FORM_EMAIL_KEY),
    title: `${body.number} - ${author} - ${title}`,
    description: get(body.data, meta.FORM_DESCRIPTION_KEY),
    path: meta.PATH,
    excludeField: meta.FORM_EXCLUDE.split(';'),
  })

  let error

  try {
    await submit(netlifyForm(body))
  } catch (e) {
    error = e.response ? e.response.data : e
    log('error', error.message || error)
  }

  if (writeLog) {
    writeLog()
  }

  cb(null, error)
}

process.on('uncaughtException', err => {
  log('error', err)

  if (writeLog) {
    writeLog()
  }
})

process.on('unhandledRejection', (reason, p) => {
  log('error', reason)
})
